include common.mk

LIBRARIES:= -lraylib -lm -l$(TARGET) -lsm
LIB_DIRS:= $(LIB_DIR) $(3RD_PARTY_LIB_DIR)
LD_FLAGS:= $(addprefix -L, $(LIB_DIRS))
OUTPUT:= $(BIN_DIR)/$(TARGET)

all: $(SRC_DIR)
	$(MAKE) --directory=$(SRC_DIR) $@
	$(CXX) -o $(OUTPUT) $(LD_FLAGS) $(LIBRARIES) $(RPATH) $(CFLAGS)

clean: $(SRC_DIR)
	$(MAKE) --directory=$(SRC_DIR) $@
	rm -rf $(OUTPUT)

run:
	$(OUTPUT)

.PHONY: all clean run
