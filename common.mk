export PROJECT_PATH:=$(PWD)
SRC_DIR:= $(PROJECT_PATH)/src
INC_DIR:= $(PROJECT_PATH)/inc
LIB_DIR:= $(PROJECT_PATH)/lib
OBJ_DIR:= $(PROJECT_PATH)/obj
BIN_DIR:= $(PROJECT_PATH)/bin
CXX_STD:= -std=c++2b
TARGET:= snake
OS_NAME:= $(shell uname -s)
3RD_PARTY_LIB_DIR:=
DEBUG_OPT:= 
EXTRA:=

ifeq ($(OS_NAME), Linux)
	INC_DIR += /usr/local/include/
	3RD_PARTY_LIB_DIR += /usr/local/lib64
	RPATH:= -Wl,-rpath=/usr/local/lib64
endif

ifeq ($(OS_NAME), Darwin)
	INC_DIR += /opt/homebrew/include/
	3RD_PARTY_LIB_DIR += /opt/homebrew/lib
endif

ifeq ($(DEBUG), TRUE)
	DEBUG_OPT:= -ggdb -O0 -DDEBUG_MODE
endif

ifeq ($(ADDRESS), TRUE)
	EXTRA += -fsanitize=address
endif


CFLAGS:= $(addprefix -I, $(INC_DIR)) $(CXX_STD) $(DEBUG_OPT) $(EXTRA)
