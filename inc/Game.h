#pragma once
#include "utils/utility.h"
#include <queue>
#include <memory>
#include <variant>

class GameObjectList;
struct NotStartedState;
struct RunningState;
struct EndState;
struct DestroyState;

using State_t =
    std::variant<NotStartedState, RunningState, EndState, DestroyState>;

class StateMachine;

class Game {
public:
  Game();
  ~Game();
  void Run();
private:
  void Reset();

private:
  struct StateHandler;

private:
  constexpr static int WINDOW_WIDTH{800};
  constexpr static int WINDOW_HEIGHT{800};
  constexpr static int INDENT_RATIO{8};
  constexpr static Vector2 WALL_START{(float)WINDOW_WIDTH / INDENT_RATIO,
                                      (float)WINDOW_HEIGHT / INDENT_RATIO};
  constexpr static Vector2 WALL_END{
      WINDOW_WIDTH - (float)WINDOW_WIDTH / INDENT_RATIO,
      WINDOW_WIDTH - (float)WINDOW_HEIGHT / INDENT_RATIO};
  constexpr static Vector2 WINDOW_CENTER{(float)WINDOW_WIDTH / 2,
                                         (float)WINDOW_HEIGHT / 2};
  constexpr static sizeType WALL_THICKNESS{3.};
  constexpr static Vector2 WALL_START_OUTLINE{WALL_START.x - WALL_THICKNESS,
                                              WALL_START.y - WALL_THICKNESS};
  constexpr static Vector2 WALL_END_OUTLINE{WALL_END.x + WALL_THICKNESS,
                                            WALL_END.y + WALL_THICKNESS};
  constexpr static char WINDOW_TITLE[]{"Snake"};
private:
  std::queue<int> moveQueue_;
  std::unique_ptr<GameObjectList> objects_;
  std::unique_ptr<State_t> gameState_;
  std::unique_ptr<StateMachine> stateMachine_;
  std::size_t score_;
  bool run_;
};
