#pragma once
#include "GameObject.h"
#include "InteractItf.h"
#include "Rect.h"
#include "utils/utility.h"

class Block;

class Wall : public GameObject, public InteractItf<Wall> {
private:
public:
  Wall(Vector2 start, Vector2 end, Color c, float thickness);
  void Draw() override final;

  bool InteractImpl(Block &w);
  Vector2 GetStart() const;
  Vector2 GetTopMostRight() const;
  Vector2 GetBottomMostLeft() const;
  Vector2 GetEnd() const;
  float GetThickness() const;

private:
  Rect rect_;
  Color color_;
  float thickness_;
};
