#pragma once
#include "utils/utility.h"

class Rect {
public:
  Rect(Vector2 start, Vector2 end);
  Rect(Vector2 start, sizeType width, sizeType height);

  Vector2 GetPosition() const;
  void SetPosition(Vector2 position);

  sizeType GetWidth() const;
  sizeType GetHeight() const;

  void Draw(Color c);
  void Draw(Color c, sizeType thickness);

private:
  Rectangle rect_;
};
