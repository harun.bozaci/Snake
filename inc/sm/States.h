#pragma once
#include <variant>

struct NotStartedState {};
struct RunningState {};
struct EndState {};
struct DestroyState {};

using State_t =
    std::variant<NotStartedState, RunningState, EndState, DestroyState>;
