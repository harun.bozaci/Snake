#pragma once
#include <cstdint>
#include <variant>

struct KeyboardEvent {
  int key;
};

struct InteractEvent {
  enum class InteractionType : uint8_t { FOOD_INT, WALL_INT, BITTEN_INT };
  InteractionType type_;
};

struct RestartEvent {};

struct TerminateEvent {};

using Event_t =
    std::variant<KeyboardEvent, InteractEvent, RestartEvent, TerminateEvent>;
