#include "States.h"
#include "TransitionTableBase.h"
#include <optional>

struct KeyboardEvent;
struct InteractEvent;
struct RestartEvent;
struct TerminateEvent;

struct DestroyStateTransitions 
    : TransitionTableBase<DestroyStateTransitions, DestroyState> {
  template <typename Event>
  std::nullopt_t Transit(const Event&) {
    return std::nullopt;
  }
};

