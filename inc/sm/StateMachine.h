#pragma once
#include "Events.h"
#include "States.h"
#include "TransitionTableBase.h"

class StateMachine {
  using TransitionTable_t =
      overloaded<struct NotStartedStateTransitions
                , struct RunningStateTransitions
                , struct EndStateTransitions
                , struct DestroyStateTransitions>;

public:
  StateMachine(State_t initialState);
  State_t ProcessEvent(const Event_t &event);
#ifdef DEBUG_MODE
  void PrintState();
#endif

private:
  State_t state_;
};
