#pragma once
#include "States.h"
#include <optional>


template <typename TransitionTable, typename State>
class TransitionTableBase {

public:
  template <typename Event>
  std::optional<State_t> operator()(const Event &event, const State &) {
    return static_cast<TransitionTable &>(*this).Transit(event);
  }
};

template <typename... Ts> struct overloaded : Ts... {
  using Ts::operator()...;
};

template <typename... Ts> overloaded(Ts...) -> overloaded<Ts...>;
