#pragma once
#include "States.h"
#include "TransitionTableBase.h"
#include <optional>

struct KeyboardEvent;
struct InteractEvent;
struct RestartEvent;
struct TerminateEvent;

struct RunningStateTransitions
    : TransitionTableBase<RunningStateTransitions, RunningState> {
  std::nullopt_t Transit(const RestartEvent &);
  State_t Transit(const KeyboardEvent &);
  std::nullopt_t Transit(const TerminateEvent &);
  State_t Transit(const InteractEvent &);
};
