#pragma once
#include "States.h"
#include "TransitionTableBase.h"
#include <optional>

struct KeyboardEvent;
struct InteractEvent;
struct RestartEvent;
struct TerminateEvent;

struct EndStateTransitions
    : TransitionTableBase<EndStateTransitions, EndState> {
  NotStartedState Transit(const RestartEvent &);
  State_t Transit(const KeyboardEvent &);
  DestroyState Transit(const TerminateEvent &);
  std::nullopt_t Transit(const InteractEvent &);
};
