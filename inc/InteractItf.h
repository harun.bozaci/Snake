#pragma once

template <typename Derived> class InteractItf {
public:
  template <typename Oth> auto Interact(InteractItf<Oth> &oth) {
    return static_cast<Oth &>(oth).InteractImpl(static_cast<Derived &>(*this));
  }
};
