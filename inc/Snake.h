#pragma once
#include "GameObject.h"
#include "InteractItf.h"
#include "utils/utility.h"
#include <memory>

class Block;
class Wall;
class Food;

class Snake : public GameObject, public InteractItf<Snake> {
public:
  Snake(Vector2 start);
  void Draw() override final;
  bool InteractImpl(Food &f);
  bool InteractImpl(Wall &w);
  bool InteractImpl(Block &block);
  bool InteractImpl(Snake &);
  void Move();
  void UpdateDirection(Direction direction);
  const std::shared_ptr<Block> GetHead() const;
  ~Snake();

private:
  void Grow();

private:
  std::shared_ptr<Block> head_;
  std::weak_ptr<Block> tail_;
};
