#pragma once
#include "GameObject.h"
#include "InteractItf.h"
#include "Rect.h"

class Snake;
class Wall;
class Block;

class Food : public GameObject, public InteractItf<Food> {

  constexpr static const sizeType foodSize{10.};

public:
  Food(Vector2 boundaryStart, Vector2 BoundaryEnd, Color c = BLACK);
  void Draw() override final;

  bool InteractImpl(Block &w);
  void InteractImpl(Snake &w);

  void SetColor(Color c);
  double GetSize() const;
  Vector2 GetPosition() const;

private:
  Vector2 Respawn();
  Vector2 CenterFoodCoordinate(Vector2 outerObjectSize, Vector2 foodCoord);

private:
  Rect rect_;
  Vector2 boundaryStart_;
  Vector2 boundaryEnd_;
  Color color_;
};
