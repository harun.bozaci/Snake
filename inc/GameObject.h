#pragma once
#include <memory>
#include <string>
#include <unordered_map>

class GameObject {
public:
  virtual ~GameObject() = default;
  virtual void Draw() = 0;
};

class GameObjectList : public GameObject {
public:
  void Draw() override final;
  void Add(std::string name, std::unique_ptr<GameObject> obj);
  std::unique_ptr<GameObject> &Get(std::string name);
  std::unique_ptr<GameObject> Delete(std::string name);

private:
  std::unordered_map<std::string, std::unique_ptr<GameObject>> list_;
};
