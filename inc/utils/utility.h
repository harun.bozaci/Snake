#pragma once
#include <raylib.h>
#include <stdint.h>

enum class Direction : uint8_t {
  UP,
  DOWN,
  RIGHT,
  LEFT,
};

using sizeType = decltype(Vector2::x);
