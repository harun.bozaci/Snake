#pragma once
#include "GameObject.h"
#include "InteractItf.h"
#include "Rect.h"
#include "utils/utility.h"

class Wall;

class Block : public GameObject, public InteractItf<Block> {
private:
  constexpr static const sizeType edgeSize_{20u};

public:
  Block(Vector2 position, Direction dir, Color color,
        std::unique_ptr<Block> next);
  void Draw() override final;

  bool InteractImpl(Block &w);

  Vector2 GetPosition() const;
  void SetPosition(Vector2 position);
  void Move(Vector2 direction);
  sizeType GetEdgeSize() const;

  void UpdateDirection(Direction dir);
  Direction GetDirection() const;

  const std::shared_ptr<Block> GetNext() const;
  std::shared_ptr<Block> GetNext();
  void SetNext(std::shared_ptr<Block> next);

private:
  std::shared_ptr<Block> next_;
  Rect rect_;
  Color color_;
  Direction dir_;
};
