#include "GameObject.h"
#include <format>
#include <iostream>
#include <stdexcept>

void GameObjectList::Draw() {
  for (const auto &[_, go] : list_) {
    go->Draw();
  }
}

void GameObjectList::Add(std::string name, std::unique_ptr<GameObject> obj) {
  if (list_.contains(name)) {
    throw std::runtime_error{"Same object name can not be used twice"};
  }

  list_.emplace(std::make_pair(name, std::move(obj)));
}

std::unique_ptr<GameObject> GameObjectList::Delete(std::string name) {
  auto iter{list_.find(name)};
  if (iter == list_.end()) {
    std::cerr << "no such object named \"" << name
              << "\" appears in the list\n";
    return nullptr;
  }
  auto ret{std::move(list_.erase(iter)->second)};
  return ret;
}

std::unique_ptr<GameObject> &GameObjectList::Get(std::string name) {
  auto iter{list_.find(name)};
  if (iter == list_.end()) {
    throw std::runtime_error{
        std::format("Object with name '{}' could not be found!", name)};
  }
  return iter->second;
}
