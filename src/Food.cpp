#include "Food.h"
#include "Block.h"
#include "Snake.h"
#include "Wall.h"
#include <cassert>
#include <random>
#include <raylib.h>

// Block and Rectangle implementation must be separate...
Food::Food(Vector2 boundaryStart, Vector2 boundaryEnd, Color c)
    : rect_{Vector2{.0, .0}, Food::foodSize, Food::foodSize}, color_{c},
      boundaryStart_{boundaryStart}, boundaryEnd_{boundaryEnd} {
  rect_.SetPosition(Respawn());
}
void Food::Draw() { rect_.Draw(color_); }

bool Food::InteractImpl(Block &block) {
  auto foodPos{rect_.GetPosition()};
  auto blockPos{block.GetPosition()};
  auto blockSize{block.GetEdgeSize()};

  return CheckCollisionRecs(Rectangle{.x = foodPos.x,
                                      .y = foodPos.y,
                                      .width = Food::foodSize,
                                      .height = Food::foodSize},
                            Rectangle{.x = blockPos.x,
                                      .y = blockPos.y,
                                      .width = blockSize,
                                      .height = blockSize});
}

void Food::InteractImpl(Snake &snake) {
  auto start{snake.GetHead()};
  auto edgeSize{start->GetEdgeSize()};
  bool intersects{true};
  Vector2 foodPos;
  while (intersects) {
    foodPos = Respawn();
    Rectangle food{.x = foodPos.x,
                   .y = foodPos.y,
                   .width = Food::foodSize,
                   .height = Food::foodSize};
    intersects = false;
    for (auto node{start}; !intersects && node != nullptr;
         node = node->GetNext()) {
      auto pos{node->GetPosition()};
      Rectangle block{
          .x = pos.x, .y = pos.y, .width = edgeSize, .height = edgeSize};
      if (CheckCollisionRecs(food, block)) {
        intersects = true;
      }
    }
  }

  rect_.SetPosition(
      CenterFoodCoordinate(Vector2{.x = edgeSize, .y = edgeSize}, foodPos));
}

Vector2 Food::Respawn() {
  static std::random_device rd;
  static std::mt19937 gen{rd()};
  std::uniform_real_distribution xDist{
      std::roundf(boundaryStart_.x / Food::foodSize) * Food::foodSize,
      std::roundf((boundaryEnd_.x - Food::foodSize) / Food::foodSize) *
          Food::foodSize};
  std::uniform_real_distribution yDist{
      std::roundf(boundaryStart_.y / Food::foodSize) * Food::foodSize,
      std::roundf((boundaryEnd_.y - Food::foodSize) / Food::foodSize) *
          Food::foodSize};
  return Vector2{xDist(gen), yDist(gen)};
}

Vector2 Food::CenterFoodCoordinate(Vector2 outerObjectSize, Vector2 foodCoord) {
  return Vector2{.x = foodCoord.x + (outerObjectSize.x - Food::foodSize) / 2,
                 .y = foodCoord.y + (outerObjectSize.y - Food::foodSize) / 2};
}

void Food::SetColor(Color c) { color_ = c; }
double Food::GetSize() const { return Food::foodSize; }
Vector2 Food::GetPosition() const { return rect_.GetPosition(); }
