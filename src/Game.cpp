#include "Game.h"
#include "Food.h"
#include "GameObject.h"
#include "Snake.h"
#include "Wall.h"
#include "sm/Events.h"
#include "sm/StateMachine.h"
#include <cctype>
#include <memory>
#include <optional>
#include <raylib.h>
#include <unordered_set>

namespace details {
using KeyType = int;
constexpr static KeyType StartKey{KEY_SPACE};
constexpr static KeyType ExitKey{KEY_ESCAPE};
constexpr static KeyType RestartKey{KEY_R};
constexpr static KeyType UpKey{KEY_W};
constexpr static KeyType DownKey{KEY_S};
constexpr static KeyType LeftKey{KEY_A};
constexpr static KeyType RightKey{KEY_D};
} // namespace details

struct Game::StateHandler {
  StateHandler(Game &gameObj) : gameObj_{gameObj} {}
  template <typename State> auto operator()(const State &st) {
    return Handle(st);
  }

  std::optional<Event_t> Handle(const NotStartedState &) {
    std::optional<Event_t> event{};
#ifndef DEBUG_MODE
    BeginDrawing();
    ClearBackground(RAYWHITE);
    DrawText("Please press 'Space' to start game.", WINDOW_CENTER.x - 100,
             WINDOW_CENTER.y, 30, GREEN);
    if (int ch{GetKeyPressed()}; ch != 0) {
      event.emplace(KeyboardEvent{.key = ch});
    }

    EndDrawing();
#else
    event.emplace(KeyboardEvent{.key = details::StartChar});
#endif
    return event;
  }

  std::optional<Event_t> Handle(const RunningState &) {

    auto &wall{static_cast<Wall &>(*gameObj_.objects_->Get("Wall"))};
    auto &snake{static_cast<Snake &>(*gameObj_.objects_->Get("Snake"))};
    auto &food{static_cast<Food &>(*gameObj_.objects_->Get("Food"))};

#ifndef DEBUG_MODE
    if (auto ch{std::toupper(GetCharPressed())}; ch != details::StartKey) {
      if (predefinedEvents_.contains(ch)) {
        return KeyboardEvent{.key = ch};
      } else if (directionKeys_.contains(ch)) {
        snake.UpdateDirection(directionKeys_.at(ch));
      }
    }
    if(WindowShouldClose()) {
      return KeyboardEvent{.key = details::ExitKey};
    }
#else
    snake.UpdateDirection(Direction::UP);
#endif

    snake.Move();

    using enum InteractEvent::InteractionType;

    if (wall.Interact(snake)) {
      return InteractEvent{.type_ = WALL_INT};
    }

    if (snake.Interact(snake)) {
      return InteractEvent{.type_ = BITTEN_INT};
    }

    if (food.Interact(snake)) {
      snake.Interact(food);
      gameObj_.score_ += 1;
    }

#ifndef DEBUG_MODE
    BeginDrawing();
    ClearBackground(RAYWHITE);
    gameObj_.objects_->Draw();
    EndDrawing();
#endif

    return std::nullopt;
  }

  std::optional<Event_t> Handle(const EndState &) {
    std::optional<Event_t> event{};

    BeginDrawing();
    ClearBackground(RAYWHITE);
    DrawText(
        "If you want to restart game please 'R' key.\nPress any key to exit!",
        WINDOW_CENTER.x - 100, WINDOW_CENTER.y, 20, RED);
    if (auto ch{GetKeyPressed()}; ch != 0) {
      if(ch == details::RestartKey) {
        gameObj_.Reset();
        event.emplace(KeyboardEvent{.key = ch});
      }
      else {
        event.emplace(TerminateEvent{});
      }
    } 
    EndDrawing();
    return event;
  }

  std::optional<Event_t> Handle(const DestroyState &) {
    gameObj_.run_ = false;
    return std::nullopt;
  }

public:
  Game &gameObj_;
  static inline const std::unordered_set<details::KeyType> predefinedEvents_{
      details::StartKey, details::ExitKey, details::RestartKey};
  static inline const std::unordered_map<details::KeyType, Direction>
      directionKeys_{std::make_pair(details::UpKey, Direction::UP),
                     std::make_pair(details::DownKey, Direction::DOWN),
                     std::make_pair(details::LeftKey, Direction::LEFT),
                     std::make_pair(details::RightKey, Direction::RIGHT)};
};

Game::Game()
    : moveQueue_{}, objects_{std::make_unique<GameObjectList>()},
      gameState_{std::make_unique<State_t>(NotStartedState{})},
      stateMachine_{std::make_unique<StateMachine>(*gameState_)}, score_{0},
      run_{true} {
  objects_->Add("Snake", std::make_unique<Snake>(WINDOW_CENTER));
  objects_->Add("Wall",
                std::make_unique<Wall>(WALL_START_OUTLINE, WALL_END_OUTLINE,
                                       GREEN, WALL_THICKNESS));
  objects_->Add("Food", std::make_unique<Food>(WALL_START, WALL_END, RED));

#ifndef DEBUG_MODE
  InitWindow(WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_TITLE);
  SetTargetFPS(24);
#else
  stateMachine_->PrintState();
#endif
}

void Game::Run() {
  while (run_) {
    auto event{std::visit(Game::StateHandler{*this}, *gameState_)};
    if (event.has_value()) {
      *gameState_ = stateMachine_->ProcessEvent(*event);
    }
#ifdef DEBUG_MODE
  stateMachine_->PrintState();
#endif
  }
}

void Game::Reset() {
  objects_.reset(new GameObjectList{});
  objects_->Add("Snake", std::make_unique<Snake>(WINDOW_CENTER));
  objects_->Add("Wall",
                std::make_unique<Wall>(WALL_START_OUTLINE, WALL_END_OUTLINE,
                                       GREEN, WALL_THICKNESS));
  objects_->Add("Food", std::make_unique<Food>(WALL_START, WALL_END, RED));
  gameState_.reset(new State_t{NotStartedState{}});
  stateMachine_.reset(new StateMachine{*gameState_});
  score_ = 0;
}

Game::~Game() {
  stateMachine_.reset();
  gameState_.reset();
  objects_.reset();
#ifndef DEBUG_MODE
  CloseWindow();
#endif
}
