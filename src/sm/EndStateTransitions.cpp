#include "sm/EndStateTransitions.h"
#include "sm/Events.h"
#include "sm/States.h"
#include <raylib.h>

NotStartedState EndStateTransitions::Transit(const RestartEvent &) {
  return NotStartedState{};
}

State_t EndStateTransitions::Transit(const KeyboardEvent &ke) {
  if (ke.key == KEY_R)
  {
    return NotStartedState{};
  }
  return DestroyState{};
}

DestroyState EndStateTransitions::Transit(const TerminateEvent &) {
	return DestroyState{};
}

std::nullopt_t EndStateTransitions::Transit(const InteractEvent &) {
	return std::nullopt;
}
