#include "sm/StateMachine.h"
#include "sm/EndStateTransitions.h"
#include "sm/NotStartedStateTransitions.h"
#include "sm/RunningStateTransitions.h"
#include "sm/DestroyStateTransitions.h"
#include "sm/States.h"

StateMachine::StateMachine(State_t initialState)
  : state_{std::move(initialState)} {

}

State_t StateMachine::ProcessEvent(const Event_t& event) {

  if(std::optional<State_t> 
      nextState{std::visit(TransitionTable_t{}, event, state_)}
      ; nextState.has_value())
  {
    state_ = *nextState;
  }
  return state_;
}

#ifdef DEBUG_MODE
#include <iostream>

void StateMachine::PrintState() {
  std::visit(overloaded{
      [](const NotStartedState&) {
        std::cout << "NotStartedState\n";
      },
      [](const RunningState&) {
        std::cout << "NotStartedState\n";
      },
      [](const DestroyState&) {
        std::cout << "NotStartedState\n";
      },
      [](const EndState&)
      {
        std::cout << "NotStartedState\n";
      }
      }, state_);
}
#endif
