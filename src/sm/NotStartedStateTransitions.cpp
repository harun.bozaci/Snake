#include "sm/NotStartedStateTransitions.h"
#include "sm/Events.h"
#include <raylib.h>

std::nullopt_t NotStartedStateTransitions::Transit(const RestartEvent &) {
  return std::nullopt;
}

State_t NotStartedStateTransitions::Transit(const KeyboardEvent &ke) {

  switch (ke.key)
  {
    case KEY_SPACE:
      return RunningState{};
    case KEY_ESCAPE:
      return DestroyState{};
  }

  return NotStartedState{};
}

std::nullopt_t NotStartedStateTransitions::Transit(const TerminateEvent &) {
	return std::nullopt;
}

std::nullopt_t NotStartedStateTransitions::Transit(const InteractEvent &) {
	return std::nullopt;
}
