#include "sm/RunningStateTransitions.h"
#include "sm/Events.h"
#include <raylib.h>

std::nullopt_t RunningStateTransitions::Transit(const RestartEvent &) {
	return std::nullopt;
}

State_t RunningStateTransitions::Transit(const KeyboardEvent &ke) {
  State_t s{RunningState{}};
  if (ke.key == KEY_ESCAPE || ke.key == KEY_R)
  {
    s.emplace<EndState>();
  }
  return s;
}

std::nullopt_t RunningStateTransitions::Transit(const TerminateEvent &) {
	return std::nullopt;
}

State_t RunningStateTransitions::Transit(const InteractEvent &ie) {
  State_t s{RunningState{}};
  switch(ie.type_)
  {
    using enum InteractEvent::InteractionType;
    case FOOD_INT:
      break;
    case WALL_INT:
      [[fallthrough]];
    case BITTEN_INT:
      s.emplace<EndState>();
      break;
  }
  return s;
}
