#include "Block.h"
#include "raylib.h"

Block::Block(Vector2 position, Direction dir, Color color,
             std::unique_ptr<Block> next)
    : next_{std::move(next)}, rect_{position, edgeSize_, edgeSize_},
      color_{color}, dir_{dir} {}
void Block::Draw() { rect_.Draw(color_); }

bool Block::InteractImpl(Block &block) {
  auto pos{rect_.GetPosition()};
  auto width{rect_.GetWidth()};
  auto height{rect_.GetHeight()};

  auto othPos{block.rect_.GetPosition()};
  auto othWidth{block.rect_.GetWidth()};
  auto othHeight{block.rect_.GetHeight()};

  return CheckCollisionRecs(
      Rectangle{.x = pos.x, .y = pos.y, .width = width, .height = height},
      Rectangle{.x = othPos.x,
                .y = othPos.y,
                .width = othWidth,
                .height = othHeight});
}

Vector2 Block::GetPosition() const { return rect_.GetPosition(); }

void Block::SetPosition(Vector2 position) { rect_.SetPosition(position); }

void Block::Move(Vector2 direction) {
  auto pos{rect_.GetPosition()};
  pos.x += direction.x;
  pos.y += direction.y;
  rect_.SetPosition(pos);
}

sizeType Block::GetEdgeSize() const { return edgeSize_; }

const std::shared_ptr<Block> Block::GetNext() const { return next_; }
std::shared_ptr<Block> Block::GetNext() { return next_; }
void Block::SetNext(std::shared_ptr<Block> next) {
  using std::swap;
  swap(next, next_);
}

Direction Block::GetDirection() const { return dir_; }

void Block::UpdateDirection(Direction dir) { dir_ = dir; }
