
#include "Wall.h"
#include "Block.h"
#include <raylib.h>

Wall::Wall(Vector2 start, Vector2 end, Color color, float thickness)
    : rect_{start, end}, color_{color}, thickness_{thickness} {}

void Wall::Draw() { rect_.Draw(color_, thickness_); }

Vector2 Wall::GetStart() const { return rect_.GetPosition(); }

Vector2 Wall::GetTopMostRight() const {
  auto pos{rect_.GetPosition()};
  return Vector2{pos.x + rect_.GetWidth(), pos.y};
}

Vector2 Wall::GetBottomMostLeft() const {
  auto pos{rect_.GetPosition()};
  return Vector2{pos.x, pos.y + rect_.GetHeight()};
}

Vector2 Wall::GetEnd() const {
  auto pos{rect_.GetPosition()};
  return Vector2{pos.x + rect_.GetWidth(), pos.y + rect_.GetHeight()};
}

float Wall::GetThickness() const { return thickness_; }
