#include "Snake.h"
#include "Block.h"
#include "Food.h"
#include "Wall.h"
#include <memory>
#include <raylib.h>
#include <stdexcept>
#include <unordered_map>

Snake::Snake(Vector2 start)
    : head_{std::make_unique<Block>(start, Direction::RIGHT, SKYBLUE, nullptr)},
      tail_{head_} {}

void Snake::Draw() {
  for (auto node{head_}; node != nullptr; node = node->GetNext()) {
    node->Draw();
  }
}

bool Snake::InteractImpl(Food &f) {
  auto retVal{head_->Interact(f)};
  if (retVal) {
    Grow();
  }
  return retVal;
}

bool Snake::InteractImpl(Wall &wall) {
  auto wallStart{wall.GetStart()};
  auto wallEnd{wall.GetEnd()};
  auto wallThickness{wall.GetThickness()};
  wallStart.x += wallThickness;
  wallStart.y += wallThickness;
  wallEnd.x -= wallThickness;
  wallEnd.y -= wallThickness;
  auto headPos{head_->GetPosition()};
  auto headSize{head_->GetEdgeSize()};
  return !CheckCollisionRecs(Rectangle{.x = headPos.x,
                                       .y = headPos.y,
                                       .width = headSize,
                                       .height = headSize},
                             Rectangle{.x = wallStart.x,
                                       .y = wallStart.y,
                                       .width = wallEnd.x - wallStart.x,
                                       .height = wallEnd.y - wallStart.y});
}

bool Snake::InteractImpl(Block &block) {
  for (auto node{head_}; node != nullptr; node = node->GetNext()) {
    block.Interact(block);
  }
  return false;
}

bool Snake::InteractImpl(Snake &) {
  for (auto node{head_->GetNext()}; node != nullptr; node = node->GetNext()) {
    if (node->Interact(*head_)) {
      return true;
    }
  }
  return false;
}

void Snake::Move() {
  using enum Direction;
  static const auto stepSize{head_->GetEdgeSize()};
  static const std::unordered_map<Direction, Vector2> steps{
      std::make_pair(UP, Vector2{0, -stepSize}),
      std::make_pair(DOWN, Vector2{0, stepSize}),
      std::make_pair(LEFT, Vector2{-stepSize, 0}),
      std::make_pair(RIGHT, Vector2{stepSize, 0}),
  };

  auto node{head_};
  auto pos{head_->GetPosition()};
  auto dir{head_->GetDirection()};
  node->Move(steps.at(head_->GetDirection()));

  for (node = head_->GetNext(); node != nullptr; node = node->GetNext()) {
    auto tempPos{node->GetPosition()};
    auto tempDirection(node->GetDirection());
    node->SetPosition(pos);
    node->UpdateDirection(dir);
    pos = tempPos;
    dir = tempDirection;
  }
}

void Snake::UpdateDirection(Direction direction) {
  using enum Direction;
  static const std::unordered_map<Direction, Direction> disallowedTransitions{
      std::make_pair(UP, DOWN), std::make_pair(DOWN, UP),
      std::make_pair(LEFT, RIGHT), std::make_pair(RIGHT, LEFT)};
  if (auto dir{head_->GetDirection()};
      disallowedTransitions.at(dir) == direction) {
    return;
  }
  head_->UpdateDirection(direction);
}

const std::shared_ptr<Block> Snake::GetHead() const { return head_; }

void Snake::Grow() {
  if (tail_.expired()) {
    throw std::runtime_error{"Unknown problem!!!"};
  }
  auto block{tail_.lock()};
  auto dir{block->GetDirection()};
  auto pos{block->GetPosition()};
  auto edgeSize{block->GetEdgeSize()};
  switch (dir) {
    using enum Direction;
  case UP:
    pos.y -= edgeSize;
    break;
  case DOWN:
    pos.y += edgeSize;
    break;
  case RIGHT:
    pos.x -= edgeSize;
    break;
  case LEFT:
    pos.x += edgeSize;
    break;
  }
  auto newBlock{std::make_shared<Block>(pos, dir, BLACK, nullptr)};
  block->SetNext(newBlock);
  tail_ = std::move(newBlock);
}

Snake::~Snake() = default;
