#include "Rect.h"
#include "raylib.h"

Rect::Rect(Vector2 start, Vector2 end)
    : rect_{start.x, start.y, end.x - start.x, end.y - start.y} {}

Rect::Rect(Vector2 start, float width, float height)
    : rect_{start.x, start.y, width, height} {}

Vector2 Rect::GetPosition() const { return Vector2{rect_.x, rect_.y}; }

void Rect::SetPosition(Vector2 position) {
  rect_.x = position.x;
  rect_.y = position.y;
}

sizeType Rect::GetWidth() const { return rect_.width; }

sizeType Rect::GetHeight() const { return rect_.height; }

void Rect::Draw(Color c) { DrawRectangleRec(rect_, c); }

void Rect::Draw(Color c, float thickness) {
  DrawRectangleLinesEx(rect_, thickness, c);
}
